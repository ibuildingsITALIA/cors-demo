var express = require('express');
var app = express();
var cors = function () {
      return function(req, res, next) {
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'token, Authorization, x-requested-with, content-type');
        next();
      }
};


app.use(cors());

app.get('/', function(req, res){ res.send('Hello World'); });
app.put('/', function(req, res){ res.send('Hello World'); });
app.post('/', function(req, res){ res.send('Hello World'); });
app.delete('/', function(req, res){ res.send('Hello World'); });


var server = app.listen(3000, function() {
      console.log('Listening on port %d', server.address().port);
});
